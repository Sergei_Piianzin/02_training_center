package sergei.piianzin.curriculum;

public class Course {
  private String title;
  private int educationHours;

  public Course(String title, int hours) {
    this.title = title;
    educationHours = hours;
  }

  public String getTitle() {
    return title;
  }

  public int getEducationHours() {
    return educationHours;
  }
}
