package sergei.piianzin.curriculum;

import java.util.List;

public class Curriculum {
  private String title;
  private Progress progress;
  private List<Course> courses;

  public Curriculum(String title, Progress progress, List<Course> courses) {
    this.title = title;
    this.progress = progress;
    this.courses = courses;
  }

  public long getCoursesHours() {
    return courses.stream().mapToInt(Course::getEducationHours).sum();
  }

  public Progress getProgress() {
    return progress;
  }

  public int getCoursesCount() {
    return courses.size();
  }

  public String getTitle() {
    return title;
  }
}
