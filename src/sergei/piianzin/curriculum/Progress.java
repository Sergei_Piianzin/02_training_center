package sergei.piianzin.curriculum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Progress {
  private List<Integer> marks;
  private boolean dismissed;
  private double average;
  private static final double GOOD_SCORE = 4.5d;
  public static final int THE_BEST_MARK = 5;

  public boolean isDismissed() {
    return dismissed;
  }

  public Progress(Integer[] marks) {
    this.marks = new ArrayList<>(Arrays.asList(marks));
    calcCurrentAverage();
    dismissed = willDismissed(average);
  }

  public Integer[] getMarks() {
    return marks.toArray(new Integer[marks.size()]);
  }

  public double getAverage() {
    calcCurrentAverage();
    return average;
  }

  private void calcCurrentAverage() {
    double sum = 0;
    for (int mark : marks) {
      sum += mark;
    }
    average = sum / marks.size();
  }

  public String getStatus(double remainingDays) {
    if (!willDismissed(getAverage())) {
      return "��� ������.";
    }
    if (canContinue(remainingDays)) {
      return "����� ���������� ��������.";
    } else {
      return "���������� ���������.";
    }
  }

  public boolean willDismissed(double average) {
    if (average < GOOD_SCORE) {
      return true;
    } else {
      return false;
    }
  }

  public boolean canContinue(double remainingDays) {
    double sum = marks.stream().mapToInt(i -> new Integer(i)).sum();

    if (remainingDays > 0.0d) {
      sum += remainingDays * Progress.THE_BEST_MARK;
    }

    double average = sum / (remainingDays + marks.size());

    if (willDismissed(average)) {
      return false;
    }
    return true;
  }
}
