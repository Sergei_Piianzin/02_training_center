package sergei.piianzin.main;

import sergei.piianzin.sorter.AccountSorter;
import sergei.piianzin.student.Student;
import sergei.piianzin.student.StudentsCreator;
import sergei.piianzin.student.StudentsSort;

public class Main {
  public static void main(String[] args) {
    AccountSorter<Student> am = new AccountSorter<>(StudentsCreator.getDefaultStudents());
    am.showAll(StudentsSort.sortedByRemainedHours);
  }
}
