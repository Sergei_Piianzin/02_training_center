package sergei.piianzin.sorter;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class AccountSorter<T> {
  private List<T> accounts;

  public AccountSorter(List<T> accounts) {
    this.accounts = accounts;
  }

  public void showAll(Comparator<T> sortedBy) {
    accounts.stream().sorted(sortedBy).forEach(System.out::println);
  }

  public void showAll(Predicate<T> filteredBy) {
    accounts.stream().filter(filteredBy).forEach(System.out::println);
  }
}
