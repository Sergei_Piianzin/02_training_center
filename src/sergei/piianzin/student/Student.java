package sergei.piianzin.student;

import sergei.piianzin.curriculum.Curriculum;
import sergei.piianzin.time.TimeCounter;

public class Student {
  private String name;
  private Curriculum curriculum;
  private TimeCounter timeCounter;

  public Student(String name, Curriculum curriculum, TimeCounter dateManager) {
    this.name = name;
    this.curriculum = curriculum;
    this.timeCounter = dateManager;
  }

  private String getMessage() {
    StringBuilder message = new StringBuilder();
    String title = curriculum.getTitle();
    String remainingHours = new Double(getRemainingHours()).toString();
    String average = new Double(getAverage()).toString();
    String status = getStatus();

    message.append(name).append(" - �� ��������� �������� �� ��������� ").append(title)
        .append(" �������� ").append(remainingHours).append(" �.\n������� ����: ").append(average)
        .append(". ").append(status).append("\n");

    return message.toString();
  }

  private String getStatus() {
    double remainingDays = timeCounter.getRemainingDays();
    return curriculum.getProgress().getStatus(remainingDays);
  }

  public boolean canContinue() {
    double remainingDays = timeCounter.getRemainingDays();
    return curriculum.getProgress().canContinue(remainingDays);
  }

  public double getAverage() {
    return curriculum.getProgress().getAverage();
  }

  public double getRemainingHours() {
    return timeCounter.getRemainingHours();
  }

  public boolean isDismissed() {
    return curriculum.getProgress().isDismissed();
  }

  @Override
  public String toString() {
    return getMessage();
  }

}
