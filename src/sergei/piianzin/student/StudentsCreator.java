package sergei.piianzin.student;

import java.time.LocalDateTime;
import java.util.ArrayList;

import sergei.piianzin.curriculum.Course;
import sergei.piianzin.curriculum.Curriculum;
import sergei.piianzin.curriculum.Progress;
import sergei.piianzin.time.TimeCounter;

public class StudentsCreator {
  public static ArrayList<Student> getDefaultStudents() {
    // TODO: Use loop for initialising
    ArrayList<Course> courses1 = new ArrayList<>();
    courses1.add(new Course("���������� Java Servlets", 8));
    courses1.add(new Course("Spring Framework", 16));
    courses1.add(new Course("Hibernate", 20));

    ArrayList<Course> courses2 = new ArrayList<>();
    courses2.add(new Course("����� ���������� Java", 12));
    courses2.add(new Course("���������� JFC/Swing", 12));
    courses2.add(new Course("���������� JAX", 8));

    ArrayList<Course> courses3 = new ArrayList<>();
    courses3.add(new Course("���������� commons", 48));
    courses3.add(new Course("Struts Framework", 24));

    Integer[] marks1 = {5, 5};
    Integer[] marks2 = {2, 5, 5, 4};
    Integer[] marks3 = {4, 5, 5, 4, 3};

    Curriculum cur1 = new Curriculum(".NET course", new Progress(marks1), courses1);
    Curriculum cur2 = new Curriculum("Java baiscs", new Progress(marks2), courses2);
    Curriculum cur3 = new Curriculum("Web design", new Progress(marks3), courses3);

    TimeCounter dateManager1 =
        new TimeCounter(LocalDateTime.now(), marks1.length, cur1.getCoursesHours());
    TimeCounter dateManager2 =
        new TimeCounter(LocalDateTime.now(), marks2.length, cur2.getCoursesHours());
    TimeCounter dateManager3 =
        new TimeCounter(LocalDateTime.now(), marks3.length, cur3.getCoursesHours());

    ArrayList<Student> students = new ArrayList<>();
    students.add(new Student("Petr Vasiliev", cur1, dateManager1));
    students.add(new Student("Ivan Susanin", cur2, dateManager2));
    students.add(new Student("Teodor Vunderlief", cur3, dateManager3));

    return students;
  }
}
