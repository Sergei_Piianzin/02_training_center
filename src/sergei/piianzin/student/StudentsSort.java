package sergei.piianzin.student;

import java.util.Comparator;
import java.util.function.Predicate;

public class StudentsSort {
  public static final Comparator<Student> sortedByAverage =
      (s1, s2) -> Double.compare(s1.getAverage(), s2.getAverage());

  public static final Comparator<Student> sortedByRemainedHours =
      (s1, s2) -> Double.compare(s1.getRemainingHours(), s2.getRemainingHours());

  public static final Predicate<Student> filteredByDismissed =
      s -> s.isDismissed() && s.canContinue();
}
