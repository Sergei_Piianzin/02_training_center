package sergei.piianzin.time;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TimeCounter {
  private LocalDateTime currentDate;
  private LocalDateTime endingDate;
  public static final int HOURS_IN_WORKDAY = 8;

  public TimeCounter(LocalDateTime beginningDate, long passedDays, long allHours) {
    currentDate = beginningDate.plusHours(passedDays * HOURS_IN_WORKDAY);
    endingDate = beginningDate.plusHours(allHours);
  }

  public double getRemainingHours() {
    double rest = ChronoUnit.HOURS.between(currentDate, endingDate);;
    if (rest < 0) {
      rest = 0;
    }
    return rest;
  }

  public double getRemainingDays() {
    double rest = ChronoUnit.HOURS.between(currentDate, endingDate) / HOURS_IN_WORKDAY;
    if (rest < 0) {
      rest = 0;
    }
    return rest;
  }
}
